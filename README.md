# Navigation Development #

This repository is implemented by Piyapat Saranrittichai.

### Completed Works ###

* nd::RgbdOdometer class : Odometry Estimation from RGBD cameras according to [this paper](https://vision.in.tum.de/_media/spezial/bib/kerl13icra.pdf).

### Ongoing Works ###

* nd::SemiDenseOdometer class : Semi-Dense Odometry Estimation from Monocular Cameras according to [this paper](https://vision.in.tum.de/_media/spezial/bib/engel2013iccv.pdf).


### Dependency ###

* Install third-party libraries by the following scripts in terminal:

```
sudo apt-get install libsuitesparse-dev libopencv-dev libboost-all-dev
```

### Installation ###

* Clone git reprository

```
#!bash

mkdir ~/ndlib
cd ~/ndlib
git clone https://pathpot@bitbucket.org/pathpot/ndlib.git
```
* Build the library as well as executables by running:

```
sh 01_compile.sh
```

### Simple Test ###

* To make a simple test if the library works fine, RGBD odometry estimation can be executed on the freiburg2_desk dataset from [TUM Computer Vision Group](http://vision.in.tum.de/). The download of the dataset can be done using the command:

```
sh 02_download_tum_dataset.sh
```

* Run RGBD odometry estimation on the downloaded dataset:

```
sh 03_demo.sh
```

There will be 3 pop-up windows. The ones labeled with "Current RGB image" and "Current depth image" are measurement of RGB and depth images from the current frame respectively. Another one labeled with "Propagated depth image from the reference frame" is the depth image transformed from the reference frame (i.e. the first frame) using transformation obtained from RGBD-Odometer algorithm. Therefore, the image perspective from this window should be close the one in "Current depth image" window. The comparison of both depth images is also displayed in the window "Depth Image Comparison".
