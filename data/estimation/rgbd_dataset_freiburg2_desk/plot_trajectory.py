import math
import numpy
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab

_EPS = numpy.finfo(float).eps * 4.0

def compute_distance(transform):
    """
    Compute the distance of the translational component of a 4x4 homogeneous matrix.
    """
    return numpy.linalg.norm(transform[0:3,3])

def compute_angle(transform):
    """
    Compute the rotation angle from a 4x4 homogeneous matrix.
    """
    # an invitation to 3-d vision, p 27
    return numpy.arccos( min(1,max(-1, (numpy.trace(transform[0:3,0:3]) - 1)/2) ))

def transform44(l):
    """
    Generate a 4x4 homogeneous transformation matrix from a 3D point and unit quaternion.
    
    Input:
    l -- tuple consisting of (stamp,tx,ty,tz,qx,qy,qz,qw) where
         (tx,ty,tz) is the 3D position and (qx,qy,qz,qw) is the unit quaternion.
         
    Output:
    matrix -- 4x4 homogeneous transformation matrix
    """
    t = l[1:4]
    q = numpy.array(l[4:8], dtype=numpy.float64, copy=True)
    nq = numpy.dot(q, q)
    if nq < _EPS:
        return numpy.array((
        (                1.0,                 0.0,                 0.0, t[0])
        (                0.0,                 1.0,                 0.0, t[1])
        (                0.0,                 0.0,                 1.0, t[2])
        (                0.0,                 0.0,                 0.0, 1.0)
        ), dtype=numpy.float64)
    q *= numpy.sqrt(2.0 / nq)
    q = numpy.outer(q, q)
    return numpy.array((
        (1.0-q[1, 1]-q[2, 2],     q[0, 1]-q[2, 3],     q[0, 2]+q[1, 3], t[0]),
        (    q[0, 1]+q[2, 3], 1.0-q[0, 0]-q[2, 2],     q[1, 2]-q[0, 3], t[1]),
        (    q[0, 2]-q[1, 3],     q[1, 2]+q[0, 3], 1.0-q[0, 0]-q[1, 1], t[2]),
        (                0.0,                 0.0,                 0.0, 1.0)
        ), dtype=numpy.float64)

def read_trajectory(filename, matrix=True):
    """
    Read a trajectory from a text file. 
    
    Input:
    filename -- file to be read
    matrix -- convert poses to 4x4 matrices
    
    Output:
    dictionary of stamped 3D poses
    """
    file = open(filename)
    data = file.read()
    lines = data.replace(","," ").replace("\t"," ").split("\n") 
    list = [[float(v.strip()) for v in line.split(" ") if v.strip()!=""] for line in lines if len(line)>0 and line[0]!="#"]
    list_ok = []
    for i,l in enumerate(list):
        if l[4:8]==[0,0,0,0]:
            continue
        isnan = False
        for v in l:
            if numpy.isnan(v): 
                isnan = True
                break
        if isnan:
            sys.stderr.write("Warning: line %d of file '%s' has NaNs, skipping line\n"%(i,filename))
            continue
        list_ok.append(l)
    if matrix :
      traj = dict([(l[0],transform44(l[0:])) for l in list_ok])
    else:
      traj = dict([(l[0],l[1:8]) for l in list_ok])
    return traj

#trajectory0 = read_trajectory("groundtruth.txt")
#trajectory0 = read_trajectory("estimation-dense-tracking-weight-temporal.txt")
#trajectory0 = read_trajectory("estimation-dense-tracking-weight-init.txt")
trajectory0 = read_trajectory("output_09_estimation.txt")

stamps0 = list(trajectory0.keys())
stamps0.sort()

transform0 = list()

x0 = list()
y0 = list()
z0 = list()
dist0 = list()
for i in range(len(stamps0)):
	transform0.append(trajectory0[stamps0[i]])
	x0.append(trajectory0[stamps0[i]][0,3])
	y0.append(trajectory0[stamps0[i]][1,3])
	z0.append(trajectory0[stamps0[i]][2,3])
	
	dx = x0[i] - x0[0];
	dy = y0[i] - y0[0];
	dz = z0[i] - z0[0];

	dist0.append(math.sqrt(dx*dx + dy*dy + dz*dz))

fig = plt.figure()
a0 = fig.add_subplot(211)
a0.plot(stamps0, x0, '-', color="red")
a0.plot(stamps0, y0, '-', color="green")
a0.plot(stamps0, z0, '-', color="blue")

a1 = fig.add_subplot(212)
a1.plot(stamps0, dist0, '-', color="black")
plt.show()
