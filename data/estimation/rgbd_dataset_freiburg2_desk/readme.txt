example command to run:

../../tum_dataset/tools/evaluate_rpe.py --fixed_delta --delta_unit 's' --plot simple.png groundtruth.txt estimation-dense-tracking-simple.txt
../../tum_dataset/tools/evaluate_rpe.py --fixed_delta --delta_unit 's' --plot weight.png groundtruth.txt estimation-dense-tracking-weight.txt

../../tum_dataset/tools/evaluate_rpe.py --fixed_delta --delta_unit 's' --plot weight.png groundtruth.txt estimation.txt
