#include <nd/core/linear_solver.h>
#include <iostream>

void nd::LinearSolver::solveCholesky(const Eigen::MatrixXd & A, const Eigen::VectorXd &b, Eigen::VectorXd &x){
	int dim = x.rows();
	assert(A.rows() == dim && A.cols() == dim);
	assert(b.rows() == dim);
	Eigen::LLT<Eigen::MatrixXd> LLT(A);
	Eigen::MatrixXd L = LLT.matrixL();
	Eigen::VectorXd c(dim);

	// solve forward substitution L*c = b
	double sum = 0;
	for(int i=0;i<dim;i++){
		// compute sum from known variable
		sum = 0;
		for(int j=0;j<i;j++){
			sum += L(i, j)*c(j);
		}

		// solve
		c(i) = (b(i) - sum)/L(i, i);
	}

	Eigen::MatrixXd LT = L.transpose();

	// solve backward substitution LT*x = c
	for(int i=dim-1;i>=0;i--){
		sum = 0;
		for(int j=dim-1;j>i;j--){
			sum += LT(i, j)*x(j);
		}

		x(i) = (c(i) - sum)/LT(i, i);
	}
}
