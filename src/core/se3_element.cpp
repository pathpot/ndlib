#include <nd/core/se3_element.h>
#include <math.h>

const double nd::Se3Element::SmallThetaThreshold = 0.0000001;

void nd::Se3Element::exp(const Eigen::Vector3d &v, const Eigen::Vector3d &w, Eigen::Matrix3d &R, Eigen::Vector3d &t){
	double theta = sqrt(w.dot(w));

	double A, B, C;
	if(theta > SmallThetaThreshold){
		A = sin(theta)/theta;
		B = (1 - cos(theta))/(theta*theta);
		C = (1 - A)/(theta*theta);
	}else{
		A = 1 - (1.0/6)*theta*theta;
		B = 0.5 - (1.0/24)*theta*theta;
		C = 1.0/6 - (1.0/120)*theta*theta;
	}

	Eigen::Matrix3d wx;
	skew_w(w, wx);
	Eigen::Matrix3d V = Eigen::Matrix3d::Identity() + B*wx + C*wx*wx;

	R = Eigen::Matrix3d::Identity() + A*wx + B*wx*wx;
	t = V*v;
}

void nd::Se3Element::log(const Eigen::Matrix3d &R, const Eigen::Vector3d &t, Eigen::Vector3d &v, Eigen::Vector3d &w){
	double theta = acos((R.trace() - 1)/2);
	double kR, kV;
	if(theta > SmallThetaThreshold){
		kR = theta/(2*sin(theta));
		kV = 1/(theta*theta)*(1 - (theta*sin(theta))/(2*(1-cos(theta))));
	}else{
		kR = 1.0/2 + (1.0/12)*theta*theta;
		kV = 1.0/12 + (1.0/720)*theta*theta;
	}

	// w can be compute from the off-diagonal elements of ln(R)
	Eigen::Matrix3d lnR = kR*(R - R.transpose());
	w(0) = lnR(2, 1);
	w(1) = lnR(0, 2);
	w(2) = lnR(1, 0);

	// v can be compute from invV
	Eigen::Matrix3d wx;
	skew_w(w, wx);
	Eigen::Matrix3d invV = Eigen::Matrix3d::Identity() - 0.5 * wx + kV * wx * wx;
	v = invV * t;
}


void nd::Se3Element::transform(const Eigen::Vector3d &src, const Eigen::Vector3d &v, const Eigen::Vector3d &w, Eigen::Vector3d &dst){
	Eigen::Matrix3d R;
	Eigen::Vector3d t;
	exp(v, w, R, t);

	dst = R*src + t;
}

void nd::Se3Element::inverse(const Eigen::Vector3d &v, const Eigen::Vector3d &w, Eigen::Vector3d &invV, Eigen::Vector3d &invW){
	Eigen::Matrix3d R, invR;
	Eigen::Vector3d t, invT;
	exp(v, w, R, t);

	invR = R.transpose();
	invT = -invR*t;
	log(invR, invT, invV, invW);
}

void nd::Se3Element::skew_w(const Eigen::Vector3d &w, Eigen::Matrix3d &wx){
	wx << 	0, -w(2), w(1),
			w(2), 0, -w(0),
			-w(1), w(0), 0;
}

void nd::Se3Element::add(const Eigen::Vector3d &v0, const Eigen::Vector3d &w0, const Eigen::Vector3d &v1, const Eigen::Vector3d &w1,
	Eigen::Vector3d &v, Eigen::Vector3d &w){
	Eigen::Matrix3d R0, R1, R;
	Eigen::Vector3d t0, t1, t;

	exp(v0, w0, R0, t0);
	exp(v1, w1, R1, t1);

	R = R0*R1;
	t = R0*t1 + t0;

	log(R, t, v, w);
}

void nd::Se3Element::subtract(const Eigen::Vector3d &v0, const Eigen::Vector3d &w0, const Eigen::Vector3d &v1, const Eigen::Vector3d &w1,
			Eigen::Vector3d &v, Eigen::Vector3d &w){

	Eigen::Vector3d invV1, invW1;
	Se3Element::inverse(v1, w1, invV1, invW1);
	Se3Element::add(invV1, invW1, v0, w0, v, w);
}
