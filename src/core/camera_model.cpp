#include <nd/core/camera_model.h>

nd::CameraModel::CameraModel(int width, int height, double fx, double fy, double cx,
		double cy, double depthFactor):
		mWidth(width), mHeight(height),
		mFx(fx), mFy(fy), mCx(cx), mCy(cy), mDepthFactor(depthFactor){
	// nothing else to do.
}

void nd::CameraModel::inverseProject(const Eigen::Vector2d &u, const double rawDepth, Eigen::Vector3d &p, int reduceScale) const{
	p(2) = rawDepth/depthFactor();
	p(0) = p(2) * (u(0) - cx(reduceScale))/fx(reduceScale),
	p(1) = p(2) * (u(1) - cy(reduceScale))/fy(reduceScale);
}

void nd::CameraModel::project(const Eigen::Vector3d &p, double &ux, double &uy, int reduceScale) const{
	ux = fx(reduceScale)*p(0)/p(2) + cx(reduceScale);
	uy = fy(reduceScale)*p(1)/p(2) + cy(reduceScale);
}

/**
 * R = rotation mapping current to target
 * t = point of current in target frame
 */
void nd::CameraModel::computeTargetEpipolarLine(const Eigen::Vector2d &u, const Eigen::Matrix3d &R,
		const Eigen::Vector3d &t, Eigen::Vector3d &epipolarLine){

	// compute essential matrix
	Eigen::Matrix3d S, E;
	Eigen::Vector3d tInv = -R.transpose()*t; // tInv = point of target in current frame
	S <<	0,	-tInv(2), tInv(1),
			tInv(2), 0, -tInv(0),
			-tInv(1), tInv(0), 0;
	E = R*S;

	// compute fundamental matrix
	Eigen::Matrix3d K, Kinv, F;
	K << fx(1),	0,		cx(1),
			0,	fy(1),	cy(1),
			0,	0,		1;
	Kinv = K.inverse();
	F = Kinv.transpose() * E * Kinv;

	// compute epipolar line
	Eigen::Vector3d uHomo(u(0), u(1), 1);
	epipolarLine = F*uHomo;

	epipolarLine = epipolarLine/epipolarLine.norm();
}

int nd::CameraModel::width(int reduceScale) const{
	return mWidth/reduceScale;
}
int nd::CameraModel::height(int reduceScale) const{
	return mHeight/reduceScale;
}
double nd::CameraModel::fx(int reduceScale) const{
	return mFx/reduceScale;
}
double nd::CameraModel::fy(int reduceScale) const{
	return mFy/reduceScale;
}
double nd::CameraModel::cx(int reduceScale) const{
	return mCx/reduceScale;
}
double nd::CameraModel::cy(int reduceScale) const{
	return mCy/reduceScale;
}
double nd::CameraModel::depthFactor() const{
	return mDepthFactor;
}
