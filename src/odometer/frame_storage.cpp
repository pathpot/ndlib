#include <nd/odometer/frame_storage.h>

nd::FrameStorage::FrameStorage(){
	// create set of free frame indices
	for(int i=0;i<mMaxNumberOfFrames;i++){
		mIndexOfFreeFrames.insert(i);
	}
}

void nd::FrameStorage::addFrame(const cv::Mat &gray, const Eigen::Vector3d &v, const Eigen::Vector3d &w){

	// ensure data size
	if(mFrameTimeLine.size() == mMaxNumberOfFrames){
		// find index to be removed
		unsigned long removeDataIndex = 0;
		for(size_t i=1;i<mFrameTimeLine.size()-1;i++){ // -1 here to prevent removing the newest frame
			if(mFrameTimeLine[i].numberOfUse < mFrameTimeLine[removeDataIndex].numberOfUse){
				removeDataIndex = i;
			}
		}

		// remove
		mFrameTimeLine.erase(mFrameTimeLine.begin() + removeDataIndex);
		mIndexOfFreeFrames.insert(removeDataIndex);
	}

	// set data
	int addDataIdx = *mIndexOfFreeFrames.begin();

	gray.copyTo(mFrameData[addDataIdx].gray);
	mFrameData[addDataIdx].v = v;
	mFrameData[addDataIdx].w = w;

	// add into timeline
	mFrameTimeLine.push_back(TimelineElement(addDataIdx));
	mIndexOfFreeFrames.erase(addDataIdx);
}

void nd::FrameStorage::markUseFrame(int timeLineIndex){
	mFrameTimeLine[timeLineIndex].numberOfUse += 1;
}

int nd::FrameStorage::nFrames() const{
	return mFrameTimeLine.size();
}

const cv::Mat& nd::FrameStorage::frameGray(int timeLineIndex) const{
	return mFrameData[mFrameTimeLine[timeLineIndex].dataIdx].gray;
}

const Eigen::Vector3d& nd::FrameStorage::frameV(int timeLineIndex) const{
	return mFrameData[mFrameTimeLine[timeLineIndex].dataIdx].v;
}

const Eigen::Vector3d& nd::FrameStorage::frameW(int timeLineIndex) const{
	return mFrameData[mFrameTimeLine[timeLineIndex].dataIdx].w;
}
