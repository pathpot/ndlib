#include <nd/odometer/rgbd_odometer.h>

#include <nd/core/se3_element.h>
#include <nd/core/linear_solver.h>

nd::RgbdOdometer::RgbdOdometer(CameraModel &cameraModel) :
	mCameraModel(cameraModel),
	mMotionSigma(6, 6),
	mNumberOfPyramids(4),
	mNumberOfIterations(20), // 20
	mHalfExtendedBorder(mCameraModel.width(1)/2),
	mRmsResidualEpsilon(0.00001), //0.03
	mWeightTDistributionDof(5),
	mWeightTVarianceEpsilon(0.1), //0.2
	mPrevSolution(6),
	mPrevSeconds(0),
	mBufIntensityI0(mCameraModel.width(1)*mCameraModel.height(1)),
	mBufPixelI0(mCameraModel.width(1)*mCameraModel.height(1), 2),
	mBufPointI0(mCameraModel.width(1)*mCameraModel.height(1), 3),
	mBufGradientI0(mCameraModel.width(1)*mCameraModel.height(1), 2),
	mBufJacobianI0(mCameraModel.width(1)*mCameraModel.height(1), 6),
	mBufWeight(mCameraModel.width(1)*mCameraModel.height(1)),
	mAbsolutePoseV(0, 0, 0),
	mAbsolutePoseW(0, 0, 0)
{
	double attitudeSd = (M_PI/9)/(sqrt(3)); // SD 20 (degree/s)
	double transSd = 0.3/sqrt(3); // SD 0.3 (m/s)

	mMotionSigma.setZero();
	mMotionSigma(0, 0) = transSd*transSd;
	mMotionSigma(1, 1) = transSd*transSd;
	mMotionSigma(2, 2) = transSd*transSd;
	mMotionSigma(3, 3) = attitudeSd*attitudeSd;
	mMotionSigma(4, 4) = attitudeSd*attitudeSd;
	mMotionSigma(5, 5) = attitudeSd*attitudeSd;
}

void nd::RgbdOdometer::init(cv::Mat rgb, cv::Mat depth, double tSeconds){
	cv::cvtColor(rgb, mPrevGray, CV_BGR2GRAY);
	depth.copyTo(mPrevDepth);
	mPrevSolution.setZero();
	mPrevSeconds = tSeconds;

	mPixelWeightScale = cv::Mat(depth.size(), CV_64FC1);

	for(int i=0;i<mPixelWeightScale.rows;i++){
		for(int j=0;j<mPixelWeightScale.cols;j++){
			mPixelWeightScale.at<double>(i, j) = 1;
		}
	}
}

void nd::RgbdOdometer::process(cv::Mat rgb, cv::Mat depth, double tSeconds, Eigen::Matrix3d &R, Eigen::Vector3d &t, double &resultResidual){
	cv::Mat gray;
	cv::cvtColor(rgb, gray, CV_BGR2GRAY);

	// compute solution
	Eigen::VectorXd curSolution(6);
	computeSolution(gray, tSeconds, curSolution, resultResidual);

	// update state
	setReferenceState(gray, depth, curSolution, tSeconds);

	// set output
	Eigen::Vector3d v(curSolution(0), curSolution(1), curSolution(2));
	Eigen::Vector3d w(curSolution(3), curSolution(4), curSolution(5));
	Se3Element::exp(v, w, R, t);

	// update internal pose
	Se3Element::add(mAbsolutePoseV, mAbsolutePoseW, v, w, mAbsolutePoseV, mAbsolutePoseW);
}


void nd::RgbdOdometer::computeSolution(const cv::Mat &gray, double tSeconds, Eigen::VectorXd &solution, double &resultResidual){
	Eigen::VectorXd curSolution(6);
	double curWeightTVariance = 1;

	// compute current sigma
	double dt = (tSeconds - mPrevSeconds);
	Eigen::MatrixXd stepMotionSigma = mMotionSigma*(dt*dt);

	// Build Pyramid
	cv::Mat *gray0Pyr = new cv::Mat[mNumberOfPyramids];
	cv::Mat *depth0Pyr = new cv::Mat[mNumberOfPyramids];
	cv::Mat *gray1Pyr = new cv::Mat[mNumberOfPyramids];
	int *reduceScalePyr = new int[mNumberOfPyramids];

	for(int lvl=0;lvl<mNumberOfPyramids;lvl++){
		if(lvl == 0){	// copy original images to pyramids
			mPrevGray.convertTo(gray0Pyr[lvl], CV_64FC1);
			mPrevDepth.convertTo(depth0Pyr[lvl], CV_64FC1);
			gray.convertTo(gray1Pyr[lvl], CV_64FC1);
			reduceScalePyr[lvl] = 1;
		}else{ // reduced images to pyramids
			cv::pyrDown(gray0Pyr[lvl-1], gray0Pyr[lvl]);
			cv::pyrDown(depth0Pyr[lvl-1], depth0Pyr[lvl]);
			cv::pyrDown(gray1Pyr[lvl-1], gray1Pyr[lvl]);
			reduceScalePyr[lvl] = 2 * reduceScalePyr[lvl - 1];
		}
	}

	// result relative pose in SE(3)
	Eigen::Vector3d v(0, 0, 0), w(0, 0, 0), dv(0, 0, 0), dw(0, 0, 0);
	Eigen::VectorXd dPose(6);
	curSolution.setZero();

	// loop variables
	cv::Mat I0, I1, d0, d1;
	int lvlWidth, lvlHeight, lvlReduceScale, lvlExtBorder;
	Eigen::MatrixXd warpJacobian(2, 6);
	double curRmsR = -1;

	for(int lvl = mNumberOfPyramids-1;lvl>=0;lvl--){
		// get level width and height
		lvlWidth = gray0Pyr[lvl].cols;
		lvlHeight = gray0Pyr[lvl].rows;
		lvlReduceScale = reduceScalePyr[lvl];
		lvlExtBorder = mHalfExtendedBorder/lvlReduceScale;

		// get a reflected image in this level
		extendBorder(gray0Pyr[lvl], I0, lvlExtBorder);
		extendBorder(depth0Pyr[lvl], d0, lvlExtBorder);
		extendBorder(gray1Pyr[lvl], I1, lvlExtBorder);

		// select pixels from I0 to be processed with these conditions:
		//	1. depth values are not zero in the image 0.
		//	2. warped pixels lie inside the frame.
		// outputs are:
		// 1. Number of valid points
		// 2. Intensity of each valid point
		// 3. Position of each valid point (in world frame)
		// 4. Gradient of each valid point in I0
		// 5. Jacobian of each valid point
		int nValidPixels = 0;
		for(int uy=0;uy<lvlHeight;uy++){
			for(int ux=0;ux<lvlWidth;ux++){
				int uxB = ux + lvlExtBorder;
				int uyB = uy + lvlExtBorder;

				double rd = d0.at<double>(uyB, uxB);
				if(rd > EPSILON){
					// store its intensity (n x 1 matrix)
					mBufIntensityI0(nValidPixels) = I0.at<double>(uyB, uxB);

					// pixel position
					mBufPixelI0(nValidPixels, 0) = ux;
					mBufPixelI0(nValidPixels, 1) = uy;

					// compute its position in world frame (n x 3 matrix)
					Eigen::Vector3d p0;
					mCameraModel.inverseProject(Eigen::Vector2d(ux, uy), rd, p0, lvlReduceScale);
					mBufPointI0(nValidPixels, 0) = p0(0);
					mBufPointI0(nValidPixels, 1) = p0(1);
					mBufPointI0(nValidPixels, 2) = p0(2);

					// compute its gradient (n x 2 matrix)
					Eigen::Vector2d gradient;
					computeGradient(I0, uxB, uyB, gradient);
					mBufGradientI0(nValidPixels, 0) = gradient(0);
					mBufGradientI0(nValidPixels, 1) = gradient(1);

					// compute its jacobian (n x 6 matrix)
					computeWarpJacobian(static_cast<double>(ux), static_cast<double>(uy),
							static_cast<double>(rd), warpJacobian, lvlReduceScale);
					for(int k=0;k<6;k++){
						mBufJacobianI0(nValidPixels, k) =
							mBufGradientI0(nValidPixels, 0)* warpJacobian(0, k) +
							mBufGradientI0(nValidPixels, 1)* warpJacobian(1, k);
					}

					// increment the number of valid points = n.
					nValidPixels++;
				}
			}
		}

		// construct variable size matrix for linear system
		Eigen::VectorXd r0(nValidPixels);
		Eigen::MatrixXd J(nValidPixels, 6), JTW(6, nValidPixels);
		for(int i=0;i<nValidPixels;i++){
			for(int j=0;j<6;j++){
				J(i, j) = mBufJacobianI0(i, j);
			}
		}

		double prevRmsR = -1, intensity1;
		for(int iter=0;iter<mNumberOfIterations;iter++){
			// warp points to pixels in I1 according to pose and compute residual
			Eigen::Matrix3d R;
			Eigen::Vector3d t;
			Se3Element::exp(v, w, R, t);
			for(int pIdx=0;pIdx<nValidPixels;pIdx++){
				// transform
				Eigen::Vector3d p0(mBufPointI0(pIdx, 0), mBufPointI0(pIdx, 1),
						mBufPointI0(pIdx, 2));
				Eigen::Vector3d p1 = R*p0 + t;

				// project
				double uxtf, uytf, uxtBf, uytBf;
				mCameraModel.project(p1, uxtf, uytf, lvlReduceScale);
				uxtBf = uxtf + lvlExtBorder;
				uytBf = uytf + lvlExtBorder;

				// calculate residual
				r0(pIdx) = bilinInterp(I1, uxtBf, uytBf) - mBufIntensityI0(pIdx);
			}

			// compute weight
			computeWeight(r0, curWeightTVariance, mBufWeight);

			// compute JT*W
			for(int i=0;i<6;i++){
				for(int j=0;j<nValidPixels;j++){
					JTW(i, j) = J(j, i) * mBufWeight(j) *
							bilinInterp(mPixelWeightScale,
									static_cast<double>(mBufPixelI0(j, 0)),
									static_cast<double>(mBufPixelI0(j, 1)));
				}
			}

			// construct linear system
			Eigen::MatrixXd A = JTW*J + stepMotionSigma.inverse();
			Eigen::VectorXd b = -JTW*r0 + stepMotionSigma.inverse()*(mPrevSolution - curSolution);

			// compute delta pose
			LinearSolver::solveCholesky(A, b, dPose);
			dv(0) = dPose(0);
			dv(1) = dPose(1);
			dv(2) = dPose(2);
			dw(0) = dPose(3);
			dw(1) = dPose(4);
			dw(2) = dPose(5);

			// update pose
			Se3Element::add(v, w, dv, dw, v, w);
			curSolution(0) = v(0);
			curSolution(1) = v(1);
			curSolution(2) = v(2);
			curSolution(3) = w(0);
			curSolution(4) = w(1);
			curSolution(5) = w(2);

			// check termination
			curRmsR = r0.norm()/nValidPixels;
			if(fabs(curRmsR - prevRmsR) < mRmsResidualEpsilon){
				break;
			}

			prevRmsR = curRmsR;
		}
	}

	// Memory Deallocation
	delete[] gray0Pyr;
	delete[] depth0Pyr;
	delete[] gray1Pyr;
	delete[] reduceScalePyr;

	// set output
	solution = curSolution;
	resultResidual = curRmsR;
}


void nd::RgbdOdometer::setReferenceState(const cv::Mat &prevGray, const cv::Mat &prevDepth, const Eigen::VectorXd &prevSolution, double prevSeconds){
	prevGray.copyTo(mPrevGray);
	prevDepth.copyTo(mPrevDepth);
	mPrevSolution = prevSolution;
	mPrevSeconds = prevSeconds;
}

void nd::RgbdOdometer::setDepthMapGaussian(cv::Mat depth, cv::Mat /*depthSigma&*/){
	depth.copyTo(mPrevDepth);
	//TODO: update Sigma
}

void nd::RgbdOdometer::extendBorder(cv::Mat &src, cv::Mat &dst, size_t borderSize){
	assert(src.channels() == 1);

	int nrow = src.rows;
	int ncol = src.cols;

	// allocate memory
	dst.release();

	if(borderSize == 0){
		src.copyTo(dst);
	}else{
		dst = cv::Mat::zeros(src.rows + 2*borderSize, src.cols + 2*borderSize, src.type());

		// Center
		src.copyTo(dst(cv::Range(borderSize, src.rows + borderSize), cv::Range(borderSize, src.cols + borderSize)));

		// Left
		for(int i=borderSize;i<borderSize + nrow;i++){
			for(int j=0;j<borderSize;j++){
				dst.at<double>(i, j) = src.at<double>(i-borderSize, borderSize - j);
			}
		}

		// Right
		for(int i=borderSize;i<borderSize + nrow;i++){
			for(int j=ncol+borderSize; j< ncol + 2*borderSize;j++){
				dst.at<double>(i, j) = src.at<double>(i-borderSize, 2*ncol + borderSize - j - 1);
			}
		}

		// Top
		for(int i=0;i<borderSize;i++){
			for(int j=0;j<dst.cols;j++){
				dst.at<double>(i, j) = dst.at<double>(2*borderSize - i, j);
			}
		}

		// Bottom
		for(int i=nrow + borderSize; i <nrow + 2*borderSize;i++){
			for(int j=0;j<dst.cols;j++){
				dst.at<double>(i, j) = dst.at<double>(2*nrow + 2*borderSize - i - 1, j);
			}
		}
	}
}

void nd::RgbdOdometer::computeGradient(cv::Mat &src, int i, int j, Eigen::Vector2d &gradient){
	double dx = bilinInterp(src, i+1, j) - bilinInterp(src, i-1, j);
	double dy = bilinInterp(src, i, j+1) - bilinInterp(src, i, j-1);

	gradient(0) = dx/2;
	gradient(1) = dy/2;

}

double nd::RgbdOdometer::bilinInterp(cv::Mat &src, double x, double y){
    double result = 0;
    int i0 = (int)floor(x);
    int j0 = (int)floor(y);
    double ai = x - i0;
    double aj = y - j0;

    if(i0 >= 0 && i0 + 1 < src.cols && j0 >= 0 && j0 + 1 < src.rows){
        result = (1-aj)*(1-ai)*src.at<double>(j0, i0) +
        		aj*(1-ai)*src.at<double>(j0 + 1, i0) +
        		(1-aj)*ai*src.at<double>(j0, i0 + 1) +
        		aj*ai*src.at<double>(j0 + 1, i0 + 1);
    }else{
    	if(i0 < 0)
    		i0 = 0;
    	else if(i0 >= src.cols)
    		i0 = src.cols - 1;

    	if(j0 < 0)
    		j0 = 0;
    	else if(j0 >= src.rows)
    		j0 = src.rows - 1;

    	result = src.at<double>(j0, i0);
    }

    return result;
}

void nd::RgbdOdometer::computeWarpJacobian(double ux, double uy, double rd, Eigen::MatrixXd &jacobian, int reduceScale) const{
	assert(jacobian.rows() == 2);
	assert(jacobian.cols() == 6);

	double fx = mCameraModel.fx(reduceScale);
	double fy = mCameraModel.fy(reduceScale);
	double cx = mCameraModel.cx(reduceScale);
	double cy = mCameraModel.cy(reduceScale);
	double df = mCameraModel.depthFactor();

	//Jacobian of target_ux respected to vx at identity
	jacobian(0, 0) = df*fx/rd;
	//Jacobian of target_ux respected to vy at identity
	jacobian(0, 1) = 0.0;
	//Jacobian of target_ux respected to vz at identity
	jacobian(0, 2) = (cx - ux)*df/rd;
	//Jacobian of target_ux respected to wx at identity
	jacobian(0, 3) = -1.0*(cx - ux)*(cy - uy)/fy;
	//Jacobian of target_ux respected to wy at identity
	jacobian(0, 4) = pow(cx - ux, 2)/fx + 1.0*fx;
	//Jacobian of target_ux respected to wz at identity
	jacobian(0, 5) = (cy - uy)*fx/fy;

	//Jacobian of target_uy respected to vx at identity
	jacobian(1, 0) = 0.0;
	//Jacobian of target_uy respected to vy at identity
	jacobian(1, 1) = df*fy/rd;
	//Jacobian of target_uy respected to vz at identity
	jacobian(1, 2) = (cy - uy)*df/rd;
	//Jacobian of target_uy respected to wx at identity
	jacobian(1, 3) = -1.0*pow(cy - uy, 2)/fy - 1.0*fy;
	//Jacobian of target_uy respected to wy at identity
	jacobian(1, 4) = (cx - ux)*(cy - uy)/fx;
	//Jacobian of target_uy respected to wz at identity
	jacobian(1, 5) = -1.0*(cx - ux)*fy/fx;
}

void nd::RgbdOdometer::computeWeight(const Eigen::VectorXd &r0, double &weightTVariance, Eigen::VectorXd &weight) const{
	int nPoints = r0.rows();
	// compute variance
	double sum = 0, r2 = 0, prevVar = weightTVariance;
	for(int iter=0;iter<10;iter++){
		sum = 0;
		for(int pIdx=0;pIdx<nPoints;pIdx++){
			r2 = r0(pIdx)*r0(pIdx);
			sum += r2*(mWeightTDistributionDof + 1)/(mWeightTDistributionDof + r2/weightTVariance);
		}

		weightTVariance = sum/nPoints;
		if(fabs(weightTVariance - prevVar) < mWeightTVarianceEpsilon)
			break;

		prevVar = weightTVariance;
	}

	// compute weight
	for(int pIdx=0;pIdx<nPoints;pIdx++){
		r2 = r0(pIdx)*r0(pIdx);
		weight(pIdx) = (mWeightTDistributionDof + 1)/(mWeightTDistributionDof + r2/weightTVariance);
	}
}

void nd::RgbdOdometer::getAbsolutePose(Eigen::Vector3d &poseV, Eigen::Vector3d &poseW){
	poseV = mAbsolutePoseV;
	poseW = mAbsolutePoseW;
}
