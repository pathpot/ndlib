#include <nd/odometer/semi_dense_odometer.h>
#include <nd/core/se3_element.h>
#include <queue>
nd::SemiDenseOdometer::SemiDenseOdometer(CameraModel &cameraModel):
	mCameraModel(cameraModel),
	mRgbdOdometer(mCameraModel),
	mStateV(0, 0, 0), mStateW(0, 0, 0),
	mIsDepthUpdate(false),
	mRefSeconds(0), mRefPose(6),
	mDepthPredictionUncertainty(0.1),
	mSsdPatternSize(5),
	mSsdSearchPixelResolution(1.5),
	mMinStereoBaseLine(0.05), // m
	mMaxStereoAngle(0.52), // rad (= 30 degrees)
	mSsdPatchWidth(mSsdPatternSize), mSsdPatchHeight(mSsdPatternSize),
	mEpilineVariance(25),
	mImageIntensityVariance(25){

	assert(mSsdPatternSize % 2 == 1);
}

void nd::SemiDenseOdometer::init(cv::Mat /*rgb0*/, cv::Mat /*depthInt0*/, double /*tSeconds0*/, cv::Mat rgb1, cv::Mat depthInt1, double tSeconds1){
	cv::Mat depth1;
	depthInt1.convertTo(depth1, CV_64FC1);

	// Set Reference - Gray
	cv::cvtColor(rgb1, mRefGray, CV_BGR2GRAY);

	// Set Reference Inverse Depth & Coordinate & Sigma
	depth1.convertTo(mRefInvDepth, CV_64FC1);
	mRefCoordinate = cv::Mat(depth1.size(), CV_64FC2);
	mRefInvDepthSigma = cv::Mat(depth1.size(), CV_64FC1);

	for(int i=0;i<mRefInvDepth.rows;i++){
		for(int j=0;j<mRefInvDepth.cols;j++){
			// inverse depth
			if(mRefInvDepth.at<double>(i, j) > EPSILON){
				mRefInvDepth.at<double>(i, j) = 5000.0/depth1.at<double>(i, j); //TODO: handle this hard code 5000
			}else{
				mRefInvDepth.at<double>(i, j) = 0;
			}

			// coordinate
			mRefCoordinate.at<cv::Vec2d>(i, j) = cv::Vec2d(i, j);

			// sigma
			mRefInvDepthSigma.at<double>(i, j) = 30.0;
			mRefInvDepthSigma.at<double>(i, j) = 30.0;
		}
	}

	// Set Reference - Solution TODO: Transformation
	mRefPose.setZero();

	// Set Reference - Seconds
	mRefSeconds = tSeconds1;

	// Set RGBD-Tracker
	cv::Mat prevDepth;
	computeInverseImage(mRefInvDepth, prevDepth);
	mRgbdOdometer.init(rgb1, prevDepth, mRefSeconds);
	mIsDepthUpdate = true;

	// Set Storage
	Eigen::Vector3d prevV(mRefPose(0), mRefPose(1), mRefPose(2));
	Eigen::Vector3d prevW(mRefPose(3), mRefPose(4), mRefPose(5));
	mFrameStorage.addFrame(mRefGray, prevV, prevW);
}

void nd::SemiDenseOdometer::process(cv::Mat rgb, double tSeconds, Eigen::Matrix3d &R, Eigen::Vector3d &t){
	// compute current pose
	double residual = 0;
	Eigen::Vector3d currentV, currentW;
	cv::Mat dummyDepth = cv::Mat::zeros(rgb.size(), CV_64FC1);
	mRgbdOdometer.process(rgb, dummyDepth, tSeconds, R, t, residual);
	mRgbdOdometer.getAbsolutePose(currentV, currentW);

	// update storage
	cv::Mat gray;
	cv::cvtColor(rgb, gray, CV_BGR2GRAY);
	mFrameStorage.addFrame(gray, currentV, currentW);

	// propagate depth
	Eigen::VectorXd currentPose(6);
	cv::Mat currentInvDepth, currentDepth, currentCoordinate, currentInvDepthSigma;
	currentPose << currentV(0), currentV(1), currentV(2),
			currentW(0), currentW(1), currentW(2);
	propagateDepthFromReference(currentPose, currentInvDepth, currentCoordinate, currentInvDepthSigma);

	// update RGBD
	computeInverseImage(currentInvDepth, currentDepth);
	mRgbdOdometer.setDepthMapGaussian(currentDepth, currentInvDepthSigma);

	// set that this depth is just from propagation
	mIsDepthUpdate = false;
}

void nd::SemiDenseOdometer::updateDepthMap(){
	const cv::Mat &currentGray = mFrameStorage.frameGray(mFrameStorage.nFrames() - 1);
	const Eigen::Vector3d &currentV = mFrameStorage.frameV(mFrameStorage.nFrames() - 1);
	const Eigen::Vector3d &currentW = mFrameStorage.frameW(mFrameStorage.nFrames() - 1);
	Eigen::VectorXd currentPose(6);
	currentPose << currentV(0), currentV(1), currentV(2), currentW(0), currentW(1), currentW(2);

	// TODO: if the current translation is far from the last depth map translation more than X cm
	if(!mIsDepthUpdate){
		// propagate inverse depth map
		cv::Mat currentInvDepth, currentCoordinate, currentInvDepthSigma;
		propagateDepthFromReference(currentPose, currentInvDepth, currentCoordinate, currentInvDepthSigma);

		// for each pixel
		cv::Mat refineInvDepth, refineCoordinate, refineInvDepthSigma;
		currentInvDepth.copyTo(refineInvDepth);
		currentCoordinate.copyTo(refineCoordinate);
		currentInvDepthSigma.copyTo(refineInvDepthSigma);

		Eigen::Vector3d c2tV, c2tW, eLine; // transformation from target to current frame
		Eigen::VectorXd eSearchPattern(mSsdPatternSize);
		double uInvDepth;
		for(int uy=0;uy<currentInvDepth.rows;uy++){
			for(int ux=0;ux<currentInvDepth.cols;ux++){
				uInvDepth = currentInvDepth.at<double>(uy, ux);
				if(uInvDepth > EPSILON){
					// find reference frame from the past frame
					bool match = false;
					Eigen::Matrix3d R; // R Current to Target
					Eigen::Vector3d t; // t Target to Current
					Eigen::Vector2d minSsdPx;
					for(int tgtFrameIdx=0;tgtFrameIdx<mFrameStorage.nFrames() - 1;tgtFrameIdx++){

						// get target frame
						const cv::Mat &targetGray = mFrameStorage.frameGray(tgtFrameIdx);
						const Eigen::Vector3d &targetV = mFrameStorage.frameV(tgtFrameIdx);
						const Eigen::Vector3d &targetW = mFrameStorage.frameW(tgtFrameIdx);

						// compute transformation to the target frame
						Se3Element::subtract(targetV, targetW, currentV, currentW, c2tV, c2tW);
						Se3Element::exp(c2tV, c2tW, R, t);

						// TODO: check frame angle & baseline
						if(t.norm() > mMinStereoBaseLine){

							// inf depth point = zero disparity point
							Eigen::Vector2d u1Inf;
							warpPointInfDepth(Eigen::Vector2d(ux, uy), c2tV, c2tW, u1Inf);

							// compute epipolar line
							mCameraModel.computeTargetEpipolarLine(Eigen::Vector2d(ux, uy), R, t, eLine);

							// compute search bound + search direction
							Eigen::Vector2d searchDirection, startPx, endPx;
							if(assignSearchBound(eLine, startPx, endPx, searchDirection)){
								// warp to an image patch on the target image
								warpPointToPattern(currentGray, Eigen::Vector2d(ux, uy), uInvDepth, c2tV, c2tW, searchDirection, eSearchPattern);

								// search this pattern along epipolar line
								int numSearchSamples = static_cast<int>(floor((endPx - startPx).norm() + 1/mSsdSearchPixelResolution));
								double newIntensity;
								Eigen::Vector2d samplePx, newIntensityPx;
								double minSsd;

								// prepare queue
								std::vector<double> currentNeighbor, chosenNeighbor;
								for(int spIdx=-4;spIdx<0;spIdx++){
									newIntensityPx = startPx + (spIdx + mSsdPatternSize/2) * mSsdSearchPixelResolution * searchDirection;
									newIntensity = bilinInterp8U(targetGray, static_cast<double>(newIntensityPx(0)), static_cast<double>(newIntensityPx(1)));
									currentNeighbor.push_back(newIntensity);
								}

								// search
								for(int spIdx=0;spIdx<numSearchSamples;spIdx++){
									samplePx = startPx + spIdx * mSsdSearchPixelResolution * searchDirection;
									newIntensityPx = startPx + (spIdx + mSsdPatternSize/2) * mSsdSearchPixelResolution * searchDirection;
									newIntensity = bilinInterp8U(targetGray, static_cast<double>(newIntensityPx(0)), static_cast<double>(newIntensityPx(1)));
									currentNeighbor.push_back(newIntensity);

									// compute SSD
									double sampleSsd = 0, dI;
									for(int i=0;i<mSsdPatternSize;i++){
										dI = (currentNeighbor[i] - eSearchPattern(i));
										sampleSsd += (dI*dI);
										chosenNeighbor = currentNeighbor;
									}

									if(spIdx == 0 || minSsd > sampleSsd){
										minSsd = sampleSsd;
										minSsdPx = samplePx;
									}

									// remove oldest sample
									currentNeighbor.erase(currentNeighbor.begin());
								}

								// Compute variance
								Eigen::Vector2d matchGradient;
								computeGradient8U(Eigen::Vector2d(minSsdPx(0), minSsdPx(1)), targetGray, matchGradient);
								double matchGeoVariance = computeGeometricVariance(matchGradient, eLine);
								double matchPhotoVariance = computePhotometricVariance(matchGradient);

								//TODO: check match criteria
								Eigen::Vector2d gradient;
								double geo, photo;
								match = true;
								for(int i=0;i<mSsdPatternSize;i++){
									samplePx = minSsdPx + (i - mSsdPatternSize/2) * mSsdSearchPixelResolution * searchDirection;
									computeGradient8U(samplePx, targetGray, gradient);
									geo = computeGeometricVariance(gradient, eLine);
									photo = computePhotometricVariance(gradient);

									if(geo < matchGeoVariance || photo < matchPhotoVariance){
										match = false;
										break;
									}
								}

//								if(match){ //to print debug
//									// triangulation in p1 frame (target frame)
//									Eigen::Vector3d p0 = t; // reference origin in target frame
//									Eigen::Vector3d p1(0, 0, 0);
//									Eigen::Vector3d v0, v1;
//									mCameraModel.inverseProject(Eigen::Vector2d(ux, uy), 1, v0, 1);
//									v0 = R*v0;
//									v0 = v0/v0.norm();
//
//									mCameraModel.inverseProject(minSsdPx, 1, v1, 1);
//									v1 = v1/v1.norm();
//
//									double l0, l1;
//									computeIntersectionLength(p0, v0, p1, v1, l0, l1);
//
//									std::cout << "Distance" << 1/uInvDepth << " " << l0 << " " << l1 << std::endl;
//
//									// print image
//									int tx = ux;
//									int ty = uy;
//
//									cv::Mat disp, cur;
//									currentGray.copyTo(cur);
//									targetGray.copyTo(disp);
//									printf("disp : %lf\n", (u1Inf - minSsdPx).norm());
//									cv::circle(cur, cv::Point2i(tx,ty), 5, cv::Scalar(255), -1);
//
//									cv::line(disp, cv::Point2i(round(double(startPx(0))), round(double(startPx(1)))), cv::Point2i(round(double(endPx(0))), round(double(endPx(1)))), cv::Scalar(255), 2);
//									cv::circle(disp, cv::Point2i(round(double(startPx(0))), round(double(startPx(1)))), 5, cv::Scalar(0), -1); // start point
//									cv::circle(disp, cv::Point2i(round(double(u1Inf(0))), round(double(u1Inf(1)))), 5, cv::Scalar(255), -1); // inf point
//									cv::circle(disp, cv::Point2i(round(double(u1Inf(0) + 30 * searchDirection(0))), round(double(u1Inf(1) + 30 * searchDirection(1)))), 5, cv::Scalar(0), -1); // direction point
//									cv::circle(disp, cv::Point2i(round(double(minSsdPx(0))), round(double(minSsdPx(1)))), 5, cv::Scalar(127), -1); // inf point
//									cv::imshow("Cur", cur);
//									cv::imshow("Disp", disp);
//									cv::waitKey();
//								}
							}
						}


						// if already match -> don't need to search the next reference frame
						if(match){
							break;
						}

					}

					if(match){
						// triangulation in p1 frame (target frame)
//						Eigen::Vector3d p0 = t; // reference origin in target frame
//						Eigen::Vector3d p1(0, 0, 0);
//						Eigen::Vector3d v0, v1;
//						mCameraModel.inverseProject(Eigen::Vector2d(ux, uy), 1, v0, 1);
//						v0 = R*v0;
//						v0 = v0/v0.norm();
//
//						mCameraModel.inverseProject(minSsdPx, 1, v1, 1);
//						v1 = v1/v1.norm();
//
//						double l0, l1;
//						computeIntersectionLength(p0, v0, p1, v1, l0, l1);


						// check uncertainty criteria
						// compute depth
						// merge into map
					}
				}

			}
		}

		// smooth

		// regularize

		// update RGBD
		cv::Mat refineDepth;
		computeInverseImage(refineInvDepth, refineDepth);
		mRgbdOdometer.setDepthMapGaussian(refineDepth, refineInvDepthSigma);

		// update Reference
		mRefPose = currentPose;
		mRefInvDepth = refineInvDepth;
		mRefCoordinate = refineCoordinate;
		mRefInvDepthSigma = refineInvDepthSigma;
		displayInverseDepth(refineInvDepth);

	}

	mIsDepthUpdate = true;
}

void nd::SemiDenseOdometer::warpPointToPattern(const cv::Mat &gray0_8U, const Eigen::Vector2d &u0,
		double depth0, const Eigen::Vector3d &v, const Eigen::Vector3d &w, const Eigen::Vector2d &searchDirection, Eigen::VectorXd &pattern){
	// project to target pixel
	Eigen::Vector3d p0, p1;
	mCameraModel.inverseProject(u0, depth0*mCameraModel.depthFactor(), p0, 1);
	Se3Element::transform(p0, v, w, p1);
	double u1x, u1y;
	mCameraModel.project(p1, u1x, u1y, 1);

	// loop variable
	Eigen::Vector3d invV, invW;
	Se3Element::inverse(v, w, invV, invW);

	// warp
	for(int i=0;i<mSsdPatternSize;i++){
		// assign prefered pixel
		int dl = i -mSsdPatternSize/2;
		double s_u1x = u1x + dl * mSsdSearchPixelResolution * searchDirection(0);
		double s_u1y = u1y + dl * mSsdSearchPixelResolution * searchDirection(1);

		// project point back to reference frame
		Eigen::Vector3d s_p0, s_p1;
		mCameraModel.inverseProject(Eigen::Vector2d(s_u1x, s_u1y),
				static_cast<double>(p1(2))*mCameraModel.depthFactor(), s_p1, 1);
		Se3Element::transform(s_p1, invV, invW, s_p0);
		double s_u0x, s_u0y;
		mCameraModel.project(s_p0, s_u0x, s_u0y, 1);

		pattern(i) = bilinInterp8U(gray0_8U, s_u0x, s_u0y);
	}
}

void nd::SemiDenseOdometer::warpPointInfDepth(const Eigen::Vector2d &u0, const Eigen::Vector3d &v,
		const Eigen::Vector3d &w, Eigen::Vector2d &u1){

	double cx = mCameraModel.cx(1);
	double cy = mCameraModel.cy(1);
	double fx = mCameraModel.fx(1);
	double fy = mCameraModel.fy(1);
	double ux = u0(0);
	double uy = u0(1);

	Eigen::Matrix3d R;
	Eigen::Vector3d t;
	Se3Element::exp(v, w, R, t);

	double u1x = (cx*fx*fy*R(0, 0) + cy*fx*fx*R(0, 1) - fx*fx*fy*R(0, 2) + cx*cx*fy*R(2, 0) + cx*cy*fx*R(2, 1) - cx*fx*fy*R(2, 2) - (fx*fy*R(0, 0) + cx*fy*R(2, 0))*ux - (fx*fx*R(0, 1) + cx*fx*R(2, 1))*uy)/
			(cx*fy*R(2, 0) + cy*fx*R(2, 1) - fx*fy*R(2, 2) - fy*R(2, 0)*ux - fx*R(2, 1)*uy);

	double u1y = (cx*fy*fy*R(1,0) + cy*fx*fy*R(1, 1) - fx*fy*fy*R(1, 2) + cx*cy*fy*R(2, 0) + cy*cy*fx*R(2, 1) - cy*fx*fy*R(2, 2) - (fy*fy*R(1, 0) + cy*fy*R(2, 0))*ux - (fx*fy*R(1, 1) + cy*fx*R(2,1))*uy)/
			(cx*fy*R(2, 0) + cy*fx*R(2, 1) - fx*fy*R(2, 2) - fy*R(2, 0)*ux - fx*R(2, 1)*uy);

	u1 << u1x, u1y;
}
void nd::SemiDenseOdometer::computeInverseImage(const cv::Mat &src, cv::Mat &dst){
	src.copyTo(dst);
	for(int i=0;i<dst.rows;i++){
		for(int j=0;j<dst.cols;j++){
			if(dst.at<double>(i ,j) > EPSILON){
				dst.at<double>(i, j) = 1/dst.at<double>(i, j);
			}
		}
	}
}

void nd::SemiDenseOdometer::propagateDepthFromReference(const Eigen::VectorXd &pose,
		cv::Mat &invDepth, cv::Mat &coordinate, cv::Mat &invDepthSigma) const{
	invDepth = cv::Mat::zeros(mRefInvDepth.size(), CV_64FC1);
	coordinate = cv::Mat::zeros(mRefInvDepth.size(), CV_64FC2);
	invDepthSigma = cv::Mat::zeros(mRefInvDepth.size(), CV_64FC1);

	// compute SE(3) from src to dst
	Eigen::Vector3d diffV, diffW;
	Se3Element::subtract(
		Eigen::Vector3d(pose(0), pose(1), pose(2)),
		Eigen::Vector3d(pose(3), pose(4), pose(5)),
		Eigen::Vector3d(mRefPose(0), mRefPose(1), mRefPose(2)),
		Eigen::Vector3d(mRefPose(3), mRefPose(4), mRefPose(5)),
		diffV, diffW);

	double depth0, depth1;
	Eigen::Vector3d p0, p1;
	double ux1, uy1, ux0, uy0;
	int i1, j1;
	for(int i0=0;i0<mRefInvDepth.rows;i0++){
		for(int j0=0;j0<mRefInvDepth.cols;j0++){
			if(mRefInvDepth.at<double>(i0, j0) > EPSILON){
				// read from coordinate
				uy0 = mRefCoordinate.at<cv::Vec2d>(i0, j0)[0];
				ux0 = mRefCoordinate.at<cv::Vec2d>(i0, j0)[1];

				// transform + project
				depth0 = 1/mRefInvDepth.at<double>(uy0, ux0);
				mCameraModel.inverseProject(Eigen::Vector2d(ux0, uy0), depth0, p0, 1);
				Se3Element::transform(p0, diffV, diffW, p1);

				mCameraModel.project(p1, ux1, uy1, 1);
				depth1 = p1(2);

				// set depth image and coordinate
				i1 = static_cast<int>(round(uy1));
				j1 = static_cast<int>(round(ux1));

				if((i1 >= 0) && (i1 < mRefInvDepth.rows) &&
						(j1 >=0) && (j1 < mRefInvDepth.cols)){
					coordinate.at<cv::Vec2d>(i1, j1) = cv::Vec2d(uy1, ux1);
					invDepth.at<double>(i1, j1) = 1/depth1;
					invDepthSigma.at<double>(i1, j1) = pow(depth1/depth0, 4) * mRefInvDepthSigma.at<double>(i0, j0) + mDepthPredictionUncertainty;
				}
			}
		}
	}
}

void nd::SemiDenseOdometer::displayInverseDepth(const cv::Mat &invDepth){
	cv::Mat depth, depthDisplay;
	invDepth.copyTo(depth);

	for(int i=0;i<depth.rows;i++){
		for(int j=0;j<depth.cols;j++){
			depth.at<double>(i, j) = 5000.0/invDepth.at<double>(i, j);
		}
	}

	depth.convertTo(depthDisplay, CV_16UC1);

	cv::imshow("Inverse Depth", depthDisplay);
}

double nd::SemiDenseOdometer::bilinInterp8U(const cv::Mat &src, double x, double y){
    double result = 0;
    int i0 = (int)floor(x);
    int j0 = (int)floor(y);
    double ai = x - i0;
    double aj = y - j0;

    if(i0 >= 0 && i0 + 1 < src.cols && j0 >= 0 && j0 + 1 < src.rows){
        result = (1-aj)*(1-ai)*src.at<unsigned char>(j0, i0) +
        		aj*(1-ai)*src.at<unsigned char>(j0 + 1, i0) +
        		(1-aj)*ai*src.at<unsigned char>(j0, i0 + 1) +
        		aj*ai*src.at<unsigned char>(j0 + 1, i0 + 1);
    }else{
    	if(i0 < 0)
    		i0 = 0;
    	else if(i0 >= src.cols)
    		i0 = src.cols - 1;

    	if(j0 < 0)
    		j0 = 0;
    	else if(j0 >= src.rows)
    		j0 = src.rows - 1;

    	result = src.at<unsigned char>(j0, i0);
    }

    return result;
}

bool nd::SemiDenseOdometer::assignSearchBound(const Eigen::Vector3d &eLine, Eigen::Vector2d &startPx, Eigen::Vector2d &endPx, Eigen::Vector2d &searchDirection, double previousDepth){
	bool result = false;

	// start-end pixels
	if(fabs(static_cast<double>(eLine(0))) <= EPSILON){ // horizontal line
		double yFix = -eLine(2)/eLine(1);

		if(yFix >= 0 && yFix < mCameraModel.height(1)){
			startPx << 0, yFix;
			endPx << mCameraModel.width(1), yFix;
			result = true;
		}
	}else if(fabs(static_cast<double>(eLine(1))) <= EPSILON){ // vertical line
		double xFix = -eLine(2)/eLine(0);

		if(xFix >= 0 && xFix < mCameraModel.width(1)){
			startPx << xFix, 0;
			endPx << xFix, mCameraModel.height(1);
			result = true;
		}
	}else{
		std::vector<std::pair<double, double> > pointPool;

		// check x = 0, y = 0, y = height - 1, x = width - 1
		double xCandidate[4] = {
				0,
				-eLine(2)/eLine(0),
				-(eLine(2) + (mCameraModel.height(1) - 1)*eLine(1))/eLine(0),
				static_cast<double>(mCameraModel.width(1) - 1)
		};
		double yCandidate[4] = {
				-eLine(2)/eLine(1),
				0,
				static_cast<double>(mCameraModel.height(1) - 1),
				-(eLine(2) + (mCameraModel.width(1) - 1)*eLine(0))/eLine(1)
		};

		// loop to check the valid one
		for(int i=0;i<4;i++){
			if(xCandidate[i] >= 0 && xCandidate[i] < mCameraModel.width(1) &&
					yCandidate[i] >= 0 && yCandidate[i] < mCameraModel.height(1)){
				pointPool.push_back(std::make_pair(xCandidate[i], yCandidate[i]));
			}
		}


		if(pointPool.size() == 2){
			startPx << pointPool[0].first, pointPool[0].second;
			endPx << pointPool[1].first, pointPool[1].second;
			result = true;
		}
	}

	// search direction
	if(result){
		searchDirection = endPx - startPx;
		searchDirection = searchDirection/searchDirection.norm();
	}


	// TODO: assign search bound according to depth as well

	return result;
}

void nd::SemiDenseOdometer::computeIntersectionLength(const Eigen::Vector3d &p0, const Eigen::Vector3d &v0,
		const Eigen::Vector3d &p1, const Eigen::Vector3d &v1, double &l0, double &l1){

	//TODO: check if line is parallel
	Eigen::Vector3d vp = v0.cross(v1);
	double normVp = vp.norm();

	Eigen::Vector3d core = -(p1 - p0).cross(vp)/(normVp*normVp);

	//compute depth
	l0 = v1.dot(core);
	l1 = v0.dot(core);
}

void nd::SemiDenseOdometer::computeGradient8U(const Eigen::Vector2d &u, const cv::Mat &img, Eigen::Vector2d &gradient){
	double x = u(0), y = u(1);
	double dx = bilinInterp8U(img, x+1, y) - bilinInterp8U(img, x-1, y);
	double dy = bilinInterp8U(img, x, y+1) - bilinInterp8U(img, x, y-1);

	gradient(0) = dx/2;
	gradient(1) = dy/2;
}

double nd::SemiDenseOdometer::computeGeometricVariance(const Eigen::Vector2d &gradient, const Eigen::Vector3d &eLine){
	Eigen::Vector2d g = gradient/gradient.norm();
	Eigen::Vector2d l(static_cast<double>(eLine(0)), static_cast<double>(eLine(1)));
	l = l/l.norm();

	double denom = g.dot(l);
	denom = denom*denom;

	return mEpilineVariance/denom;
}

double nd::SemiDenseOdometer::computePhotometricVariance(const Eigen::Vector2d &gradient){
	double denom = gradient.norm();
	denom = denom*denom;

	return 2*mImageIntensityVariance/denom;
}



/**
 * TODO:
 * 1. implement process
 * 2. implement propagate
 * 3. check
 * 4. compute epipolar
 * 5. search epipolor + compute depth
 * 6. check
 * 7. merge into map + check
 * 8. smooh + check
 * 9. regularize + check
 */
