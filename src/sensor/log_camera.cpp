#include <nd/sensor/log_camera.h>
#include <sstream>

bool nd::LogCamera::open(std::string datasetDir){
	bool result = false;
	mDatasetDir = datasetDir;

	std::string descFilename = mDatasetDir + "/associate.txt";
	mDescriptorFile.open(descFilename.c_str());
	if(mDescriptorFile.is_open()){
		result = true;
	}else{
		printf("Cannot find %s\n", datasetDir.c_str());
	}

	return result;
}

bool nd::LogCamera::read(cv::Mat &frame){
	bool result = false;
	double timestamp;
	std::string imageFilename;

	std::string line;
	std::stringstream stream;
	while(std::getline(mDescriptorFile, line)){
		trim(line);

		if(line.at(0) != '#'){
			stream .str(line);
			stream >> timestamp;
			stream >> imageFilename;
			break;
		}
	}

	if(!imageFilename.empty()){
		imageFilename = mDatasetDir + "/" + imageFilename;
		frame = cv::imread(imageFilename);
		result = !frame.empty();
	}

	return result;
}

bool nd::LogCamera::read(cv::Mat &rgb, cv::Mat &depth, double &timestamp){
	bool result = false;
	double t0, t1;
	std::string rgbImageFilename, depthImageFilename;

	std::string line;
	std::stringstream stream;
	while(std::getline(mDescriptorFile, line)){
		trim(line);

		if(line.at(0) != '#'){
			stream .str(line);
			stream >> t0;
			stream >> rgbImageFilename;
			stream >> t1;
			stream >> depthImageFilename;
			timestamp = (t0 + t1)/2;
			break;
		}
	}

	if(!rgbImageFilename.empty() && !depthImageFilename.empty()){
		rgbImageFilename = mDatasetDir + "/" + rgbImageFilename;
		rgb = cv::imread(rgbImageFilename);
		depthImageFilename = mDatasetDir + "/" + depthImageFilename;
		depth = cv::imread(depthImageFilename, CV_16UC1);

		if(rgb.empty()){
			printf("Cannot find %s\n", rgbImageFilename.c_str());
		}else if(depth.empty()){
			printf("Cannot find %s\n", depthImageFilename.c_str());
		}else{
			result = true;
		}
	}

	return result;
}

void nd::LogCamera::trim(std::string &s){
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
    s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
}
