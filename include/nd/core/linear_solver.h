#ifndef ND_LINEARSOLVER_H
#define ND_LINEARSOLVER_H

#include <Eigen/Dense>

namespace nd{
	/**
	 * Tool for solving linear equation.
	 */
	class LinearSolver{
	public:
		/**
		 * Solve Ax = b where A is symmetric and positive-definite.
		 */
		static void solveCholesky(const Eigen::MatrixXd & A, const Eigen::VectorXd &b, Eigen::VectorXd &x);
	};
}

#endif
