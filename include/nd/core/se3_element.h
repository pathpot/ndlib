#ifndef ND_SE3_ELEMENT_H
#define ND_SE3_ELEMENT_H

#include <Eigen/Dense>

namespace nd{

/**
 * Class for manipulating SE3.
 *
 * The formulae in this class are adopted from the article "Lie Groups for 2D and 3D Transformations"
 * by Ethan Eade (see http://ethaneade.com/lie.pdf).
 */
class Se3Element{
public:
	/// The threshold to decide whether Taylor's series is adopt or not.
	static const double SmallThetaThreshold;

	/**
	 * SE3 exponential function.
	 * \param v Input velocity in SE3
	 * \param w Input angular velocity in SE3
	 * \param R Output rotation matrix
	 * \param t Output translation vector
	 */
	static void exp(const Eigen::Vector3d &v, const Eigen::Vector3d &w, Eigen::Matrix3d &R, Eigen::Vector3d &t);

	/**
	 * SE3 logarithm function.
	 * \param R Input rotation matrix
	 * \param t Input translation vector
	 * \param v Output velocity in SE3
	 * \param w Output angular velocity in SE3
	 */
	static void log(const Eigen::Matrix3d &R, const Eigen::Vector3d &t, Eigen::Vector3d &v, Eigen::Vector3d &w);

	/**
	 * SE3 inverse function.
	 * \param v Input velocity in SE3
	 * \param w Input angular velocity in SE3
	 * \param invV Output inverse-velocity in SE3
	 * \param invW Output inverse-angular velocity in SE3
	 */
	static void inverse(const Eigen::Vector3d &v, const Eigen::Vector3d &w, Eigen::Vector3d &invV, Eigen::Vector3d &invW);

	/**
	 * Transforming point src to dst according to SE3 element.
	 *
	 * This operation is equivalent to computing R and t from exp function and computing dst = R*src + t.
	 *
	 * \param src Input point
	 * \param v Input SE3 velocity component of the transformation
	 * \param w Input SE3 angular velocity component of the transformation
	 * \param dst Ouput transformed point
	 */
	static void transform(const Eigen::Vector3d &src, const Eigen::Vector3d &v, const Eigen::Vector3d &w, Eigen::Vector3d &dst);

	/**
	 * Compute multiplication under SE3 Group.
	 *
	 * H = H0 * H1
	 *
	 */
	static void add(const Eigen::Vector3d &v0, const Eigen::Vector3d &w0, const Eigen::Vector3d &v1, const Eigen::Vector3d &w1,
		Eigen::Vector3d &v, Eigen::Vector3d &w);

	/**
	 * Computing subtraction under SE3 Group.
	 *
	 * H = H1inv * H0
	 */
	static void subtract(const Eigen::Vector3d &v0, const Eigen::Vector3d &w0, const Eigen::Vector3d &v1, const Eigen::Vector3d &w1,
			Eigen::Vector3d &v, Eigen::Vector3d &w);
private:
	/**
	 * Compute skew matrix of w.
	 */
	static void skew_w(const Eigen::Vector3d &w, Eigen::Matrix3d &wx);
};

}

#endif
