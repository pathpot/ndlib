#ifndef ND_CAMERA_MODEL_H
#define ND_CAMERA_MODEL_H

#include <Eigen/Dense>

namespace nd{

/**
 * Tool for projecting points between RGBD camera frame and world frame given its intrinsic parameters.
 */
class CameraModel{
public:
	/**
	 * Constructor.
	 */
	CameraModel(int width, int height, double fx, double fy, double cx, double cy, double depthFactor);

	/**
	 * Project a 3D point in CAM frame onto the image plane.
	 *
	 * \param p An input 3D point in CAM frame
	 * \inout u An output 2D pixel on the image plane whose origin is at top-right corner.
	 */
	void project(const Eigen::Vector3d &p, double &ux, double &uy, int reduceScale) const;

	/**
	 * Converse a pixel together with its depth to the 3D point.
	 *
	 * \param u A 2D pixel
	 * \param rawDepth Depth of u as raw data before scaling
	 * \inout An output 3D point in CAM frame
	 */
	void inverseProject(const Eigen::Vector2d &u, const double rawDepth, Eigen::Vector3d &p, int reduceScale) const;
	
	/**
	 * u is a point in the reference frame
	 * R, t are transformation from the reference frame to the target frame with the same calibration matrix
	 */
	void computeTargetEpipolarLine(const Eigen::Vector2d &u, const Eigen::Matrix3d &R, const Eigen::Vector3d &t, Eigen::Vector3d &epipolarLine);

	// get functions for intrinsic parameters
	int width(int reduceScale) const;
	int height(int reduceScale) const;
	double fx(int reduceScale) const;
	double fy(int reduceScale) const;
	double cx(int reduceScale) const;
	double cy(int reduceScale) const;
	double depthFactor() const;

private:
	const int mWidth, mHeight;
	const double mFx, mFy, mCx, mCy, mDepthFactor;
};

}


#endif
