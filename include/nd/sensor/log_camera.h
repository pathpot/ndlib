#ifndef ND_LOG_CAMERA_H
#define ND_LOG_CAMERA_H

#include <fstream>
#include "camera_interface.h"

namespace nd{

/**
 * Implementation of the interface of RGBD camera reading from log file.
 */
class LogCamera : public CameraInterface{
public:
	bool open(std::string datasetDir);
	virtual bool read(cv::Mat &frame);

	bool read(cv::Mat &rgb, cv::Mat &depth, double &timestamp);

private:
	static void trim(std::string &s);

	std::string mDatasetDir;
	std::ifstream mDescriptorFile;

};


}

#endif
