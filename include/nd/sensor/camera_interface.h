#ifndef ND_CAMERA_INTERFACE_H
#define ND_CAMERA_INTERFACE_H

#include <opencv2/opencv.hpp>

namespace nd{

/**
 * Interface to RGBD camera
 */
class CameraInterface{
public:
	virtual ~CameraInterface();
	virtual bool read(cv::Mat &frame) = 0;
};

}

#endif
