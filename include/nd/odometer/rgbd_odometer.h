#ifndef ND_RGBD_ODOMETER_H
#define ND_RGBD_ODOMETER_H

#include <opencv2/opencv.hpp>
#include <nd/core/camera_model.h>

#define EPSILON 1e-10 // to compare if a double variable is zero.

namespace nd{

/**
 * Class to perform RGBD tracking according to the paper from C. Kerl.
 */
class RgbdOdometer{
public:
	/**
	 * Constructor.
	 */
	RgbdOdometer(CameraModel &cameraModel);

	/**
	 * Initializing with the first frame.
	 *
	 * This frame will be used as the reference frame. The solution transformation will be computed relative to this frame.
	 *
	 * \param rgb Input 3-channels of uint 8-Bits RGB image of the first frame.
	 * \param depth Input 1-channel of uint 16-Bits raw depth image of the first frame without correcting scales.
	 * \param tSeconds Input timestamp in seconds of the first frame.
	 */
	void init(cv::Mat rgb, cv::Mat depth, double tSeconds);

	/**
	 * Process transformation from the current frame.
	 *
	 * \param rgb Input 3-channels of uint 8-Bits RGB image of the current frame.
	 * \param depth Input 1-channel of uint 16-Bits raw depth image of the current frame without correcting scales.
	 * \param tSeconds Input timestamp in seconds of the current frame.
	 * \param R Output rotation matrix relative to the first frame.
	 * \param t Output translation vector relative to the first frame.
	 * \param resultResidual Output intensity residual from the estimation.
	 */
	void process(cv::Mat rgb, cv::Mat depth, double tSeconds, Eigen::Matrix3d &R, Eigen::Vector3d &t, double &resultResidual);

	/**
	 * Set variance of depth estimation.
	 *
	 * This will weight each depth inputs by their variance. The variances can be treated as constants in RGBD tracking. However,
	 * in some use cases (e.g. for semi-dense tracking), these values should be adjustable.
	 */
	void setDepthMapGaussian(cv::Mat depth, cv::Mat depthSigma);

	/**
	 * Get absolute pose from the estimation.
	 *
	 * \param poseV Output pose SE3 velocity.
	 * \param poseW Output pose SE3 angular velocity.
	 */
	void getAbsolutePose(Eigen::Vector3d &poseV, Eigen::Vector3d &poseW);

private:
	/**
	 * Estimate transformation from the reference frame.
	 */
	void computeSolution(const cv::Mat &gray, double tSeconds, Eigen::VectorXd &solution, double &resultResidual);

	/**
	 * Set reference frame.
	 */
	void setReferenceState(const cv::Mat &prevGray, const cv::Mat &prevDepth, const Eigen::VectorXd &prevSolution, double prevSeconds);

	/**
	 * Create border of the image.
	 *
	 * \param src source image.
	 * \param dst target image.
	 * \param borderSize
	 */
	static void extendBorder(cv::Mat &src, cv::Mat &dst, size_t borderSize);
	static void computeGradient(cv::Mat &src, int ux, int uy, Eigen::Vector2d &gradient);

	/**
	 * Get image intensity in the sub-pixel level using bilinear interpolation.
	 */
	static double bilinInterp(cv::Mat &src, double x, double y);

	/**
	 * Compute 6D Jacobian of the warp function with respect to the pose at pose = 0.
	 *
	 * \inout jacobian a 2 x 6 Matrix of Jacobian
	 */
	void computeWarpJacobian(double ux, double uy, double rd, Eigen::MatrixXd &jacobian, int reduceScale) const;

	/**
	 * Update weight
	 */
	void computeWeight(const Eigen::VectorXd &r0, double &weightTVariance, Eigen::VectorXd &weight) const;

	// member variables
	CameraModel &mCameraModel;
	Eigen::MatrixXd mMotionSigma;

	// optimization parameters
	int mNumberOfPyramids;
	int mNumberOfIterations;
	int mHalfExtendedBorder;
	double mRmsResidualEpsilon;

	int mWeightTDistributionDof;
	double mWeightTVarianceEpsilon;

	// previous state to track the next frame
	cv::Mat mPrevGray, mPrevDepth;
	cv::Mat mPixelWeightScale;
	Eigen::VectorXd mPrevSolution;
	double mPrevSeconds;

	// buffer variable for optimization
	Eigen::VectorXd mBufIntensityI0;
	Eigen::MatrixXd mBufPixelI0;
	Eigen::MatrixXd mBufPointI0;
	Eigen::MatrixXd mBufGradientI0;
	Eigen::MatrixXd mBufJacobianI0;
	Eigen::VectorXd mBufWeight;

	// internal absolute pose
	Eigen::Vector3d mAbsolutePoseV, mAbsolutePoseW;
};

}

#endif
