#ifndef ND_FRAME_STORAGE_H
#define ND_FRAME_STORAGE_H

#include <opencv2/opencv.hpp>
#include <Eigen/Dense>
#include <set>

namespace nd{

/**
 * An entry of frame stored in the storage.
 */
typedef struct FrameEntry{
	/// RGB image
	cv::Mat gray;

	// location
	Eigen::Vector3d v, w;
}FrameEntry;

/**
 * A couple (dataIdx, numberOfUse) tracking number of use of each frame index.
 */
typedef struct TimelineElement{
	int dataIdx;
	int numberOfUse;

	TimelineElement(int idx, int numUse = 0) : dataIdx(idx), numberOfUse(numUse){

	}
}TimelineElement;

/**
 * Frame Storage created exclusively for keeping track of past frames in semi-dense tracking.
 */
class FrameStorage{
public:
	/**
	 * Constructor.
	 */
	FrameStorage();

	/**
	 * Add new frame.
	 *
	 * A past frame which is used least will be deleted if the number of frames is greater than mMaxNumberOfFrames.
	 *
	 * \param gray New frame
	 * \param v New frame SE3 velocity
	 * \param w New frame SE3 angular velocity
	 */
	void addFrame(const cv::Mat &gray, const Eigen::Vector3d &v, const Eigen::Vector3d &w);

	/**
	 * Increment the number of use of the specified index by one.
	 */
	void markUseFrame(int timeLineIndex);

	/**
	 * Get number of frame.
	 */
	int nFrames() const;

	/**
	 * Get frame of the specified index entry.
	 */
	const cv::Mat& frameGray(int timeLineIndex) const;

	/**
	 * Get velocity of the specified index entry.
	 */
	const Eigen::Vector3d& frameV(int timeLineIndex) const;

	/**
	 * Get angular velocity of the specified index entry.
	 */
	const Eigen::Vector3d& frameW(int timeLineIndex) const;
private:
	/**
	 * Maximum number of frames in the storage.
	 */
	static const int mMaxNumberOfFrames = 100;

	/// Storage contains (frameDataIndex, numberOfUse).
	std::vector<TimelineElement> mFrameTimeLine;

	/// Frame data.
	FrameEntry mFrameData[mMaxNumberOfFrames];

	/// Keeping track of the free frame indices.
	std::set<int> mIndexOfFreeFrames;
};
}


#endif
