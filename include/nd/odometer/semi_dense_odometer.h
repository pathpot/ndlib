#ifndef ND_SEMI_DENSE_ODOMETER_H
#define ND_SEMI_DENSE_ODOMETER_H

#include <opencv2/opencv.hpp>
#include <nd/core/camera_model.h>
#include "frame_storage.h"
#include "rgbd_odometer.h"

typedef struct EpipolarLine{

}EpipolarLine;

namespace nd{

/**
 * Implementation of the semi-dense tracker according to the work of J. Engel.
 */
class SemiDenseOdometer{
public:
	/**
	 * Constructor.
	 */
	SemiDenseOdometer(CameraModel &cameraModel);

	/**
	 * Initialize the estimation by the first two frames.
	 *
	 * In fact, this should be able to initialize without depth image. However, during the development, depth images are given to ensure
	 * the correctness of the initialization.
	 */
	void init(cv::Mat rgb0, cv::Mat depthInt0, double tSeconds0, cv::Mat rgb1, cv::Mat depthInt1, double tSeconds1);

	/**
	 * Process the transformation from current frames.
	 *
	 * This function is only estimate the transformation from the latest available estimated depth. No depth value changes.
	 */
	void process(cv::Mat rgb, double tSeconds, Eigen::Matrix3d &R, Eigen::Vector3d &t);

	/**
	 * Compute depth values of the current frame and uses it as the reference frame.
	 */
	void updateDepthMap();

private:
	/**
	 * Compute multiplication-inverse of the 1-channel double src image.
	 *
	 * This function could be used for inverse-depth related operations.
	 */
	static void computeInverseImage(const cv::Mat &src, cv::Mat &dst);

	/**
	 * Get a pixel value within sub-pixel accuracy by bilinear interpolation.
	 */
	static double bilinInterp8U(const cv::Mat &src, double x, double y);

	/**
	 * Propagate depth image from the reference frame to the current frame from purely geometric transformation.
	 */
	void propagateDepthFromReference(const Eigen::VectorXd &pose, cv::Mat &invDepth, cv::Mat &coordinate, cv::Mat &invDepthSigma) const;

	/**
	 * Update the current frame to reference frame
	 */
	void updateReferenceFrame();

	/**
	 * Display image of inverse depth matrix.
	 */
	void displayInverseDepth(const cv::Mat &invDepth);

	/**
	 * Compute warped patch in the target image (1) from the source image (0)
	 * \param v SE3 velocity from source image to target image
	 * \param w SE3 angular velocity from source image to target image
	 */
	void warpPointToPattern(const cv::Mat &img0, const Eigen::Vector2d &u0, double depth0,
			const Eigen::Vector3d &v, const Eigen::Vector3d &w, const Eigen::Vector2d &searchDirection, Eigen::VectorXd &pattern);

	/**
	 * Warp point u0 at infinity depth from the reference frame to the point u1 at the frame with specified transformation.
	 */
	void warpPointInfDepth(const Eigen::Vector2d &u0, const Eigen::Vector3d &v, const Eigen::Vector3d &w,
			Eigen::Vector2d &u1);

	/**
	 * Assign search bound on the epipolar line.
	 */
	bool assignSearchBound(const Eigen::Vector3d &eLine, Eigen::Vector2d &startPx, Eigen::Vector2d &endPx, Eigen::Vector2d &searchDirection, double previousDepth = 0);

	/**
	 * Compute intersection between p0 + l0*v0 and p1 + l1*v1
	 */
	void computeIntersectionLength(const Eigen::Vector3d &p0, const Eigen::Vector3d &v0,
			const Eigen::Vector3d &p1, const Eigen::Vector3d &v1, double &l0, double &l1);

	/**
	 * Compute gradient info
	 */
	void computeGradient8U(const Eigen::Vector2d &u, const cv::Mat &img, Eigen::Vector2d &gradient);

	/**
	 * Compute geometric variance of the current pixel.
	 */
	double computeGeometricVariance(const Eigen::Vector2d &gradient, const Eigen::Vector3d &eLine);

	/**
	 * Compute photometric variance of the current pixel.
	 */
	double computePhotometricVariance(const Eigen::Vector2d &gradient);

	// components
	CameraModel &mCameraModel;
	RgbdOdometer mRgbdOdometer;
	FrameStorage mFrameStorage;

	// configuration parameters
	const double mDepthPredictionUncertainty;
	const int mSsdPatternSize;
	const double mSsdSearchPixelResolution;
	const int mSsdPatchWidth, mSsdPatchHeight;
	const double mMinStereoBaseLine, mMaxStereoAngle;
	const double mEpilineVariance, mImageIntensityVariance;

	// current states
	Eigen::Vector3d mStateV, mStateW;
	bool mIsDepthUpdate;

	// reference frames
	cv::Mat mRefGray;
	cv::Mat mRefInvDepth;
	cv::Mat mRefCoordinate;
	cv::Mat mRefInvDepthSigma;
	Eigen::VectorXd mRefPose;
	double mRefSeconds;

};
}

#endif
