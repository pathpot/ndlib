
def skew_w(w):
	wx = matrix([[0, -w[2, 0], w[1, 0]],
		[w[2, 0], 0, -w[0, 0]],
		[-w[1, 0], w[0, 0], 0]])
	return wx

# exp function of se3 element estimated by tailor series expansion at identity.
def se3_exp(v, w):
	theta = sqrt(w[0, 0]^2 + w[1, 0]^2 + w[2, 0]^2)
	A = 1 - (1.0/6)*theta*theta;
	B = 0.5 - (1.0/24)*theta*theta;
	C = 1.0/6 - (1.0/120)*theta*theta;
	wx = skew_w(w)
	R = matrix.identity(3) + A*wx + B*wx*wx
	V = matrix.identity(3) + B*wx + C*wx*wx
	t = V*v
	return R, t

def inv_project(ux, uy, rd, fx, fy, cx, cy, df):
	pz = rd/df
	px = pz * (ux - cx)/fx
	py = pz * (uy - cy)/fy
	p = matrix([[px], [py], [pz]])
	return p
	
def project(p, fx, fy, cx, cy):
	ux = fx*p[0, 0]/p[2, 0] + cx
	uy = fy*p[1, 0]/p[2, 0] + cy
	return ux, uy

def transform(p0, R, t):
	p1 = R*p0 + t
	return p1

def warp_x(ux, uy, rd, v, w, fx, fy, cx, cy, df):
	# inv_project
	p0 = inv_project(ux, uy, rd, fx, fy, cx, cy, df)
	
	# exp
	R, t = se3_exp(v, w)
	
	# transform
	p1 = transform(p0, R, t)
	
	# project
	target_ux, target_uy = project(p1, fx, fy, cx, cy)
	
	return target_ux.simplify(), target_uy.simplify()

# SE3 element variables
var('vx vy vz wx wy wz')
v = matrix([[vx], [vy], [vz]])
w = matrix([[wx], [wy], [wz]])

# camera parameters
var('fx fy cx cy df')

# measurement
var('ux uy rd')

# warp
target_ux, target_uy = warp_x(ux, uy, rd, v, w, fx, fy, cx, cy, df)

print('Jacobian of target_ux respected to vx at identity')
show(target_ux.diff(vx).subs(vx = 0, vy = 0, vz = 0, wx = 0, wy = 0, wz = 0))
print('Jacobian of target_ux respected to vy at identity')
show(target_ux.diff(vy).subs(vx = 0, vy = 0, vz = 0, wx = 0, wy = 0, wz = 0))
print('Jacobian of target_ux respected to vz at identity')
show(target_ux.diff(vz).subs(vx = 0, vy = 0, vz = 0, wx = 0, wy = 0, wz = 0))
print('Jacobian of target_ux respected to wx at identity')
show(target_ux.diff(wx).subs(vx = 0, vy = 0, vz = 0, wx = 0, wy = 0, wz = 0))
print('Jacobian of target_ux respected to wy at identity')
show(target_ux.diff(wy).subs(vx = 0, vy = 0, vz = 0, wx = 0, wy = 0, wz = 0))
print('Jacobian of target_ux respected to wz at identity')
show(target_ux.diff(wz).subs(vx = 0, vy = 0, vz = 0, wx = 0, wy = 0, wz = 0))
print('')
print('Jacobian of target_uy respected to vx at identity')
show(target_uy.diff(vx).subs(vx = 0, vy = 0, vz = 0, wx = 0, wy = 0, wz = 0))
print('Jacobian of target_uy respected to vy at identity')
show(target_uy.diff(vy).subs(vx = 0, vy = 0, vz = 0, wx = 0, wy = 0, wz = 0))
print('Jacobian of target_uy respected to vz at identity')
show(target_uy.diff(vz).subs(vx = 0, vy = 0, vz = 0, wx = 0, wy = 0, wz = 0))
print('Jacobian of target_uy respected to wx at identity')
show(target_uy.diff(wx).subs(vx = 0, vy = 0, vz = 0, wx = 0, wy = 0, wz = 0))
print('Jacobian of target_uy respected to wy at identity')
show(target_uy.diff(wy).subs(vx = 0, vy = 0, vz = 0, wx = 0, wy = 0, wz = 0))
print('Jacobian of target_uy respected to wz at identity')
show(target_uy.diff(wz).subs(vx = 0, vy = 0, vz = 0, wx = 0, wy = 0, wz = 0))