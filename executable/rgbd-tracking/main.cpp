#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <nd/sensor/log_camera.h>
#include <nd/core/camera_model.h>
#include <nd/core/se3_element.h>
#include <nd/odometer/rgbd_odometer.h>
#include <boost/program_options.hpp>


namespace po = boost::program_options;

void inverseDepthToDepth(const cv::Mat &invDepth, cv::Mat &depth){
    cv::Mat depthDouble;
    invDepth.copyTo(depthDouble);

    for(int i=0;i<depthDouble.rows;i++){
        for(int j=0;j<depthDouble.cols;j++){
            depthDouble.at<double>(i, j) = 5000.0/invDepth.at<double>(i, j);
        }
    }

    depthDouble.convertTo(depth, CV_16UC1);
}

void propagateDepthFromReference(nd::CameraModel cameraModel, const Eigen::VectorXd &refPose, const cv::Mat &refInvDepth, const Eigen::VectorXd &pose, cv::Mat &invDepth){
    invDepth = cv::Mat::zeros(refInvDepth.size(), CV_64FC1);

    // compute SE(3) from src to dst
    Eigen::Vector3d invV0, invW0, diffV, diffW;
    nd::Se3Element::inverse(
        Eigen::Vector3d(refPose(0), refPose(1), refPose(2)),
        Eigen::Vector3d(refPose(3), refPose(4), refPose(5)),
        invV0, invW0);
    nd::Se3Element::add(
        invV0, invW0,
        Eigen::Vector3d(pose(0), pose(1), pose(2)),
        Eigen::Vector3d(pose(3), pose(4), pose(5)),
        diffV, diffW);

    double depth0, depth1;
    Eigen::Vector3d p0, p1;
    double ux1, uy1, ux0, uy0;
    int i1, j1;
    for(int i0=0;i0<refInvDepth.rows;i0++){
        for(int j0=0;j0<refInvDepth.cols;j0++){
            if(refInvDepth.at<double>(i0, j0) > EPSILON){
                uy0 = i0;
                ux0 = j0;

                // transform + project
                depth0 = 1/refInvDepth.at<double>(i0, j0);
                cameraModel.inverseProject(Eigen::Vector2d(ux0, uy0), depth0, p0, 1);
                nd::Se3Element::transform(p0, diffV, diffW, p1);

                cameraModel.project(p1, ux1, uy1, 1);
                depth1 = p1(2);


                // set depth image
                i1 = static_cast<int>(round(uy1));
                j1 = static_cast<int>(round(ux1));

                if((i1 >= 0) && (i1 < refInvDepth.rows) &&
                        (j1 >=0) && (j1 < refInvDepth.cols)){
                    invDepth.at<double>(i1, j1) = 1/depth1;
                }
            }
        }
    }
}

void updateState(const Eigen::Vector3d &stateX0, const Eigen::Quaterniond &stateQ0,
        const Eigen::Vector3d &dv, const Eigen::Vector3d &dw,
        Eigen::Vector3d &v, Eigen::Vector3d &w,
        Eigen::Vector3d &stateX, Eigen::Quaterniond &stateQ){

    // update transformation locally
    nd::Se3Element::add(v, w, dv, dw, v, w);

    // exponential function
    Eigen::Matrix3d R;
    Eigen::Vector3d t;
    nd::Se3Element::exp(v, w, R, t);

    // update pose
    stateX = R*stateX0 + t;
    stateQ = Eigen::Quaterniond(stateQ0.toRotationMatrix()*R);
}

void displayDepthMultipleWindow(cv::Mat &current, cv::Mat &propagate){
    if(propagate.type() != current.type()){
        std::cerr << "Cannot display multiple depth window" << std::endl;
    }else{
        // create output image
        cv::Size sizeLeft = current.size();
        cv::Size sizeRight = propagate.size();
        cv::Mat output(sizeLeft.height, sizeLeft.width + sizeRight.width, propagate.type());

        // copy left and right images to output
        cv::Mat leftOutput(output, cv::Rect(0, 0, sizeLeft.width, sizeLeft.height));
        current.copyTo(leftOutput);
        cv::Mat rightOutput(output, cv::Rect(sizeLeft.width, 0, sizeRight.width, sizeRight.height));
        propagate.copyTo(rightOutput);

        // add text
        cv::putText(leftOutput, "Current", cv::Point2i(10, 30), cv::FONT_HERSHEY_SIMPLEX, 1, cv::Scalar(0xFFFF), 2);
        cv::putText(rightOutput, "Propagated", cv::Point2i(10, 30), cv::FONT_HERSHEY_SIMPLEX, 1, cv::Scalar(0xFFFF), 2);

        // display output
        cv::imshow("Depth Image Comparison", output);
    }
}

struct ProgramOptions {
    std::string rgbd_dataset_dir;
    std::string output_estimation_file;
    std::string output_residual_file;
};

po::options_description defineProgramOptions(ProgramOptions& program_options){
    po::options_description description("Compute and accumulate RGBD odometry");
    description.add_options()
      ("help,h", "Display help messages")
      ("rgbd-dataset-dir", po::value<std::string>(&program_options.rgbd_dataset_dir)->default_value("data/tum_dataset/rgbd_dataset_freiburg2_desk"),
       "Path to the RGBD directory dataset")
      ("output-estimation-file", po::value<std::string>(&program_options.output_estimation_file)->default_value("estimation.txt"),
       "Path to the output estimation file")
      ("output-residual-file", po::value<std::string>(&program_options.output_residual_file)->default_value("residual.txt"),
       "Path to the output residual file");

    return description;
}

void parseProgramOptions(int argc, char** argv,
                                   const po::options_description& description,
                                   ProgramOptions& /*program_options*/) {
    po::variables_map vm;
    try {
        po::store(po::parse_command_line(argc, argv, description), vm);
        po::notify(vm);
    } catch(std::exception& e) {
        std::cerr << "Cannot parse command arguments with error: "
                  << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }

    if (vm.count("help")) {
        std::cout << description << std::endl;
        exit(EXIT_SUCCESS);
    }
}

int main(int argc, char** argv){
    ProgramOptions program_options;
    auto description = defineProgramOptions(program_options);
    parseProgramOptions(argc, argv, description, program_options);

    // open camera
    nd::LogCamera camera;
    if(!camera.open(program_options.rgbd_dataset_dir)){
        std::cerr << "Cannot load image data" << std::endl;
        exit(EXIT_FAILURE);
    }

    // open estimation file
    FILE *estFile = NULL, *resFile;
    estFile = fopen(program_options.output_estimation_file.c_str(), "w");
    if(estFile == NULL){
        std::cerr << "Cannot open file for estimated trajectory." << std::endl;
        exit(EXIT_FAILURE);
    }
    resFile = fopen(program_options.output_residual_file.c_str(), "w");
    if(resFile == NULL){
        std::cerr << "Cannot open file for residual trajectory." << std::endl;
        exit(EXIT_FAILURE);
    }

    // setup RGBD odometry estimation
    // TODO: Currently the camera model is hard-coded for TUM rgbd_dataset_freiburg2_desk dataset
    nd::CameraModel cameraModel(640, 480, 525, 525, 319.5, 239.5, 5000);
    nd::RgbdOdometer odometer(cameraModel);

    // declare state variables (the initial state [X0, Q0] is assigned to identity)
    Eigen::Vector3d stateX(0, 0, 0), stateX0(0, 0, 0);
    Eigen::Quaterniond stateQ, stateQ0;
    stateQ.setIdentity();
    stateQ0.setIdentity();

    // declare other loop variables
    double timeSec;
    cv::Mat rgb, depth, estimatedDepth;
    int frameNumber = 0;
    Eigen::Matrix3d R;
    Eigen::Vector3d t, v(0, 0, 0), w(0, 0, 0), dv(0, 0, 0), dw(0, 0, 0);
    double residual = -1;


    // depth image
    cv::Mat refInvDepth, invDepth;

    // main loop
    while(true){
        if(!camera.read(rgb, depth, timeSec)){
            break;
        }

        frameNumber++;
        std::cout << "frame : " << frameNumber << std::endl;

        if(frameNumber == 1){
            // initialize state in the first frame to identity
            odometer.init(rgb, depth, timeSec);
            stateX = stateX0;
            stateQ = stateQ0;

            // initialize invDepth image in the first frame from the sensor
            cv::Mat depthDouble;
            depth.convertTo(depthDouble, CV_64FC1);
            depthDouble.copyTo(refInvDepth);
            for(int i=0;i<depth.rows;i++){
                for(int j=0;j<depth.cols;j++){
                    // inverse depth
                    if(depthDouble.at<double>(i, j) > EPSILON){
                        refInvDepth.at<double>(i, j) = cameraModel.depthFactor()/depthDouble.at<double>(i, j);
                    }else{
                        refInvDepth.at<double>(i, j) = 0;
                    }
                }
            }

        }else{
            // compute RGBD odometry
            odometer.process(rgb, depth, timeSec, R, t, residual);
            nd::Se3Element::log(R, t, dv, dw);

            // accumulate odometry
            updateState(stateX0, stateQ0, dv, dw, v, w, stateX, stateQ);
            printf("relative:\t %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf\n", dv(0), dv(1), dv(2),
                    dw(0), dw(1), dw(2));
            printf("accumulate:\t %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf\n", v(0), v(1), v(2),
                    w(0), w(1), w(2));
        }

        // compute propagated inverse depth from the first fram
        Eigen::VectorXd refPose(6);
        refPose << 0, 0, 0, 0, 0, 0;
        Eigen::VectorXd pose(6);
        pose << v(0), v(1), v(2), w(0), w(1), w(2);

        propagateDepthFromReference(nd::CameraModel(cameraModel.width(1), cameraModel.height(1),
                cameraModel.fx(1), cameraModel.fy(1), cameraModel.cx(1), cameraModel.cy(1), 1),
                refPose, refInvDepth, pose, invDepth);
        inverseDepthToDepth(invDepth, estimatedDepth);

        // print
        fprintf(estFile, "%.6lf %.6lf %.6lf %.6lf %.6lf %.6lf %.6lf %.6lf\n", timeSec, stateX(0), stateX(1), stateX(2),
                stateQ.x(), stateQ.y(), stateQ.z(), stateQ.w());
        fprintf(resFile, "%.10lf\n", residual);

        // display
        cv::imshow("Current RGB image", rgb);
        cv::imshow("Current depth image", depth);
        cv::imshow("Propagated depth image from the reference frame", estimatedDepth);
        displayDepthMultipleWindow(depth, estimatedDepth);

        if(cv::waitKey(10) == 27){
            break;
        }
    }

    fclose(estFile);
    fclose(resFile);

    std::cout << "Done." << std::endl;
    return 0;
}
