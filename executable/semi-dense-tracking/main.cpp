#include <stdio.h>
#include <nd/sensor/log_camera.h>
#include <nd/core/camera_model.h>
#include <nd/core/se3_element.h>
#include <nd/odometer/semi_dense_odometer.h>

void updateState(const Eigen::Vector3d &stateX0, const Eigen::Quaterniond &stateQ0,
		const Eigen::Vector3d &dv, const Eigen::Vector3d &dw,
		Eigen::Vector3d &v, Eigen::Vector3d &w,
		Eigen::Vector3d &stateX, Eigen::Quaterniond &stateQ){

	// update transformation locally
	nd::Se3Element::add(v, w, dv, dw, v, w);

	// exponential function
	Eigen::Matrix3d R;
	Eigen::Vector3d t;
	nd::Se3Element::exp(v, w, R, t);

	// update pose
	stateX = R*stateX0 + t;
	stateQ = Eigen::Quaterniond(stateQ0.toRotationMatrix()*R);
}

int main(){
	printf("Dense Tracking\n");

	// open camera
	nd::LogCamera camera;
	if(!camera.open("data/tum_dataset/rgbd_dataset_freiburg2_desk")){
		printf("Cannot load image data.\n");
		exit(1);
	}

	// open estimation file
	FILE *estFile = NULL, *resFile;
	estFile = fopen("data/estimation.txt", "w");
	if(estFile == NULL){
		printf("Cannot open file for estimated trajectory.\n");
		exit(1);
	}
	resFile = fopen("data/residual.txt", "w");
	if(resFile == NULL){
		printf("Cannot open file for residual trajectory.\n");
		exit(1);
	}

	// setup odometer
	nd::CameraModel cameraModel(640, 480, 525, 525, 319.5, 239.5, 1);
	nd::SemiDenseOdometer odometer(cameraModel);

	// init state
	Eigen::Vector3d stateX(0, 0, 0), stateX0(0, 0, 0);
	Eigen::Quaterniond stateQ, stateQ0;
	stateQ.setIdentity();
	stateQ0.setIdentity();

	// loop variables
	double timeSec, firstTimeSec;
	cv::Mat rgb, depth, firstRgb, firstDepth;
	int frameNumber = 0;
	Eigen::Matrix3d R;
	Eigen::Vector3d t, v(0, 0, 0), w(0, 0, 0), dv(0, 0, 0), dw(0, 0, 0);
	double residual = -1;

	// main loop
	while(true){
		if(!camera.read(rgb, depth, timeSec)){
			break;
		}

		frameNumber++;
		printf("frame : %d\n", frameNumber);

		if(frameNumber == 1){
			rgb.copyTo(firstRgb);
			depth.copyTo(firstDepth);
			firstTimeSec = timeSec;

			stateX = stateX0;
			stateQ = stateQ0;
		}else if(frameNumber == 2){
			odometer.init(firstRgb, firstDepth, firstTimeSec, rgb, depth, timeSec);
		}else{
			odometer.process(rgb, timeSec, R, t);
			odometer.updateDepthMap();
			nd::Se3Element::log(R, t, dv, dw);
			updateState(stateX0, stateQ0, dv, dw, v, w, stateX, stateQ);
			printf("relative:\t %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf\n", dv(0), dv(1), dv(2),
					dw(0), dw(1), dw(2));
			printf("accumulate:\t %.4lf %.4lf %.4lf %.4lf %.4lf %.4lf\n", v(0), v(1), v(2),
					w(0), w(1), w(2));
		}

		// print
		fprintf(estFile, "%.6lf %.6lf %.6lf %.6lf %.6lf %.6lf %.6lf %.6lf\n", timeSec, stateX(0), stateX(1), stateX(2),
				stateQ.x(), stateQ.y(), stateQ.z(), stateQ.w());
		fprintf(resFile, "%.10lf\n", residual);

		// display
		cv::imshow("rgb", rgb);
		cv::imshow("depth", depth);

		if(cv::waitKey(10) == 27){
			break;
		}
	}

	fclose(estFile);
	fclose(resFile);
	printf("End\n");

	return 0;
}
