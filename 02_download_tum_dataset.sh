#!/bin/bash

cd data/tum_dataset

# download
wget http://vision.in.tum.de/rgbd/dataset/freiburg2/rgbd_dataset_freiburg2_desk.tgz

# extract
tar -xvzf rgbd_dataset_freiburg2_desk.tgz

cd -
