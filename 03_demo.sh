#!/bin/bash

./build/bin/rgbd-tracking --rgbd-dataset-dir data/tum_dataset/rgbd_dataset_freiburg2_desk --output-estimation-file data/estimation.txt --output-residual-file data/residual.txt
